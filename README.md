# KryptoBot
Diplomová práce - KryptoBot

V souboru Config.kt je potřeba nastavit přístupové údaje pro Binance účet:

```kotlin
val BinanceAPIKey = "xxx"
val BinanceSecretKey = "xxx"
```

Celý projekt se sestavuje pomocí Gradle. Příkaz pro spuštění bota:

```kotlin
gradle run
```

