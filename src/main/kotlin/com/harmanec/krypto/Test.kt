package com.harmanec.krypto

import com.harmanec.krypto.data.DataReader
import com.harmanec.krypto.tester.Tester
import com.harmanec.krypto.tester.decider.MovingAverageCrossDecider
import com.harmanec.krypto.tester.runner.*
import kotlin.concurrent.thread
import kotlin.math.min

/**
 * Main
 *
 * Run tests
 *
 * @param args
 */
fun main(args: Array<String>) {

    val dataReader = DataReader("data/BTCUSDT/btc-20-21.csv")

    val tests: List<Pair<String, TestRunner>> = listOf(
        Pair("ma", MATestRunner(dataReader)),
        Pair("maCross", MACrossTestRunner(dataReader)),
        Pair("wma", WMATestRunner(dataReader)),
        Pair("wmaCross", WMACrossTestRunner(dataReader)),
        Pair("hma", HMATestRunner(dataReader)),
        Pair("hmaCross", HMACrossTestRunner(dataReader)),
        Pair("rsi", RSITestRunner(dataReader)),
        Pair("so", SOTestRunner(dataReader)),
        Pair("bb", BBTestRunner(dataReader)),
        Pair("sar", SARTestRunner(dataReader))
    )

    Test.runTests(tests)
}

class Test {
    companion object {
        fun runTests(tests: List<Pair<String, TestRunner>>) {
            for (runner in tests) {
                thread {
                    runner.second.runTests(
                        9,
                        3,
                        logFile = runner.first
                    )
                }
            }
        }

        fun runTest() {
            val dataReader = DataReader("data/BTCUSDT/btc-2021.csv")
            val prices = dataReader.getHourlyTickers(8)

            val tester = Tester.Builder()
                .data(prices)
                .decider(
                    MovingAverageCrossDecider(
                        prices.subList(0, min(2, prices.size - 1)),
                        2,
                        10
                    )
                )
                .logFile("wma-trades.csv")
                .longStrategy(10.0, 0.96)
                .shortStrategy(0.0, 1.04)
                .fee(0.9995)
                .build()

            val profit = tester.test()

            println("Final amount: $$profit")
        }
    }
}