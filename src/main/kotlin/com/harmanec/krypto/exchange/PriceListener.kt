package com.harmanec.krypto.exchange

import com.harmanec.krypto.model.Ticker
import com.njkim.reactivecrypto.core.common.model.currency.Currency

interface PriceListener {
    fun priceChange(currency: Currency, price: Ticker)
}