package com.harmanec.krypto.exchange

import com.binance.client.RequestOptions
import com.binance.client.SyncRequestClient
import com.binance.client.exception.BinanceApiException
import com.binance.client.impl.BinanceApiInternalFactory
import com.binance.client.model.enums.OrderSide
import com.binance.client.model.enums.OrderType
import com.binance.client.model.enums.PositionSide
import com.binance.client.model.enums.TimeInForce
import com.binance.client.model.trade.Order
import com.harmanec.krypto.Config
import com.harmanec.krypto.logger.FileLogger
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

class Binance {
    companion object {
        private const val tradingPair = "BTCUSDT"
        private const val leverage = 2
        private val timeInForce: TimeInForce = TimeInForce.GTC

        private val client: SyncRequestClient

        private const val priceRound = 1
        private const val quantityRound = 3
        private val priceFormatter = DecimalFormat("0.00")
        private val quantityFormatter = DecimalFormat("0.000")

        init {
            client = BinanceApiInternalFactory.getInstance().createSyncRequestClient(
                Config.BinanceAPIKey,
                Config.BinanceSecretKey,
                RequestOptions()
            )

            client.changeInitialLeverage(tradingPair, leverage)
        }

        fun getBalance(coin: String) : BigDecimal {
            val balances = client.balance

            for (balance in balances) {
                if (balance.asset.equals(coin)) {
                    return BigDecimal(balance.balance.toDouble())
                }
            }

            return BigDecimal.ZERO
        }

        fun placeOrder(orderId: String, side: OrderSide, quantity: BigDecimal, orderType: OrderType, price: BigDecimal?, stopPrice: BigDecimal?) : Order? {
            try {
                FileLogger.log("Placing an order: id - $orderId, side - $side, quantity: ${quantityFormatter.format(quantity.setScale(quantityRound, RoundingMode.DOWN))}, orderType: $orderType, price: ${if (orderType == OrderType.LIMIT) priceFormatter.format(price?.setScale(priceRound, RoundingMode.HALF_DOWN)) else null}, stopPrice: ${if (orderType == OrderType.STOP_MARKET || orderType == OrderType.TAKE_PROFIT_MARKET) priceFormatter.format(stopPrice?.setScale(
                    priceRound, RoundingMode.HALF_DOWN)) else null}")

                return client.postOrder(
                    tradingPair,
                    side,
                    PositionSide.BOTH,
                    orderType,
                    if (orderType == OrderType.MARKET) null else timeInForce,
                    quantityFormatter.format(quantity.setScale(quantityRound, RoundingMode.DOWN)),
                    if (orderType == OrderType.LIMIT && price != null) priceFormatter.format(price.setScale(priceRound, RoundingMode.HALF_DOWN)) else null,
                    "false",
                    orderId,
                    if ((orderType == OrderType.STOP_MARKET || orderType == OrderType.TAKE_PROFIT_MARKET) && stopPrice != null) priceFormatter.format(stopPrice.setScale(
                        priceRound, RoundingMode.HALF_DOWN)) else null,
                    null,
                    null
                )
            } catch (e: BinanceApiException) {
                FileLogger.log("Error placing an order: ${e.localizedMessage}")
            }

            return null
        }
    }
}

enum class BinancePosition {
    NONE,
    SHORT,
    LONG
}