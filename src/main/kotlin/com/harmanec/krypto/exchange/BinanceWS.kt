package com.harmanec.krypto.exchange

import com.binance.client.RequestOptions
import com.binance.client.impl.BinanceApiInternalFactory
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.harmanec.krypto.Config
import com.harmanec.krypto.logger.FileLogger
import com.harmanec.krypto.model.UserWSResponse
import okhttp3.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class BinanceWS(private var listener: BinanceWSListener?) : WebSocketListener() {
    private var running = true
    private val restClient = BinanceApiInternalFactory.getInstance().createSyncRequestClient(
        Config.BinanceAPIKey,
        Config.BinanceSecretKey,
        RequestOptions()
    )
    private var webSocket: WebSocket? = null
    private var listenKey: String
    private val service : ScheduledExecutorService

    init {
        listenKey = getListenKey()
        openWebsocket(listenKey)

        service = Executors.newSingleThreadScheduledExecutor()
        service.scheduleAtFixedRate({
            if (running) {
                restClient.keepUserDataStream(listenKey)
            } else {
                restClient.closeUserDataStream(listenKey)
                service.shutdown()
            }
        }, 1, 30, TimeUnit.MINUTES)
    }

    fun closeWebsockets() {
        running = false
        webSocket?.close(1000, null)
        service.shutdown()
        listener = null
    }

    private fun openWebsocket(key: String) {
        webSocket = OkHttpClient().newWebSocket(
            Request.Builder().url("wss://fstream-auth.binance.com/ws/${key}?listenKey=${key}").build(),
            this
        )
    }

    private fun getListenKey() : String
            = restClient.startUserDataStream()

    override fun onMessage(webSocket: WebSocket?, text: String?) {
        if (text != null) {
            try {
                val message = Gson().fromJson(text, UserWSResponse::class.java)
                when (message.e) {
                    EventType.ORDER_TRADE_UPDATE.value -> {
                        val order = message.o?.toOrder()
                        order?.let {
                            it.updateTime = message.E
                            this.listener?.orderChange(it)
                        }
                    }
                    EventType.LISTEN_KEY_EXIPIRED.value -> {
                        this.webSocket?.close(1000, null)
                        openWebsocket(getListenKey())
                    }
                    else -> {
                        // empty
                    }
                }
                FileLogger.log(message.toString())
            } catch (e: JsonSyntaxException) {
                FileLogger.log("Error parsing json: ${e.message}")
            }
        }
    }

    /**
     * Invoked when a web socket has been closed due to an error reading from or writing to the
     * network. Both outgoing and incoming messages may have been lost. No further calls to this
     * listener will be made.
     */
    override fun onFailure(webSocket: WebSocket?, t: Throwable?, response: Response?) {
        this.webSocket?.close(1000, null)
        openWebsocket(getListenKey())
    }
}

enum class EventType(val value: String) {
    LISTEN_KEY_EXIPIRED("listenKeyExpired"),
    MARGIN_CALL("MARGIN_CALL"),
    ACCOUNT_UPDATE("ACCOUNT_UPDATE"),
    ORDER_TRADE_UPDATE("ORDER_TRADE_UPDATE"),
    ACCOUNT_CONFIG_UPDATE("ACCOUNT_CONFIG_UPDATE")
}