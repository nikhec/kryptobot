package com.harmanec.krypto.exchange

import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.utils.Utils
import com.njkim.reactivecrypto.core.common.model.currency.Currency
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PriceAggregator(private val currency: Currency) : PriceListener {
    private var secondsCounter : Int = 1
    private var minutesCounter : Int = 1
    private var hoursCounter : Int = 1
    private var daysCounter: Int = 1
    private val secondlyData : MutableList<Ticker> = ArrayList(60)
    private val minutelyData : MutableList<Ticker> = ArrayList(60)
    private val hourlyData : MutableList<Ticker> = ArrayList(24)
    private val dailyData : MutableList<Ticker> = ArrayList(365)
    private val listeners : MutableMap<TimeInterval, MutableMap<Int, MutableList<AggregatedPriceListener>>> = EnumMap(TimeInterval::class.java)

    init {
        TimeInterval.values().forEach { interval ->
            listeners[interval] = HashMap()
        }
    }

    override fun priceChange(currency: Currency, price: Ticker) {
        if (this.currency == currency) {
            secondsCounter++
            secondlyData.add(price)

            notifyListeners(TimeInterval.SECOND, secondsCounter, secondlyData)

            if (secondsCounter % 60 == 0) {
                minutesCounter++
                minutelyData.add(price)
                secondlyData.clear()

                notifyListeners(TimeInterval.MINUTE, minutesCounter, minutelyData)

                secondsCounter = 0
            }
            if (minutesCounter % 60 == 0) { // hours
                hoursCounter++
                hourlyData.add(Utils.aggregateTickers(minutelyData.subList(minutelyData.size - 59, minutelyData.size)))
                minutelyData.clear()

                notifyListeners(TimeInterval.HOUR, hoursCounter, hourlyData)

                minutesCounter = 1
            }
            if (hoursCounter % 24 == 0) { // days
                daysCounter++
                dailyData.add(Utils.aggregateTickers(hourlyData.subList(hourlyData.size - 23, hourlyData.size)))
                hourlyData.clear()

                notifyListeners(TimeInterval.DAY, daysCounter, dailyData)

                hoursCounter = 1
                if (daysCounter >= Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_YEAR)) {
                    daysCounter = 1
                    dailyData.clear()
                }
            }
        }
    }

    private fun notifyListeners(timeInterval: TimeInterval, counter: Int, data: List<Ticker>) {
        listeners[timeInterval]!!.keys.forEach { key ->
            if (counter % key == 0) {
                listeners[timeInterval]!![key]!!.forEach { listener ->
                    listener.priceChange(
                        currency,
                        Utils.aggregateTickers(data.subList(data.size - key, data.size))
                    )
                }
            }
        }
    }

    /**
     * Sets listener that gets notified every step interval ex. step = 4, interval = HOURS - the listener will be
     * notified every 4 hours
     * @param listener
     * @param interval minute / hour / day
     * @param step
     */
    fun addListener(listener: AggregatedPriceListener, interval: TimeInterval, step: Int = 1) {
        if (!listeners[interval]!!.contains(step)) {
            listeners[interval]!![step] = ArrayList()
        }

        listeners[interval]!![step]!!.add(listener)
    }
}

enum class TimeInterval {
    SECOND,
    MINUTE,
    HOUR,
    DAY
}