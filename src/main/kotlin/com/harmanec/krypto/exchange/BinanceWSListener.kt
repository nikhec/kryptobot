package com.harmanec.krypto.exchange

import com.binance.client.model.trade.Order

interface BinanceWSListener {
    /**
     * Called when order is changed. For example filled or closed
     */
    fun orderChange(order: Order)
}