package com.harmanec.krypto.exchange

import com.binance.client.model.enums.OrderSide
import com.binance.client.model.enums.OrderType

class FutureTrader(private val exchangeMonitor: ExchangeMonitor) : Trader {

    private val binanceTrader = BinanceTrader()

    override fun longCoin() {
        val balance = Binance.getBalance("USDT")
        val bestPrice = exchangeMonitor.getBestBuyingPrice(balance)
        val amount = balance / bestPrice

        binanceTrader.placeOrder(
            OrderSide.BUY,
            amount,
            OrderType.LIMIT,
            bestPrice
        )
    }

    override fun shortCoin() {
        val balance = Binance.getBalance("USDT")
        val bestPrice = exchangeMonitor.getBestSellingPrice(balance)
        val amount = balance / bestPrice

        binanceTrader.placeOrder(
            OrderSide.SELL,
            amount,
            OrderType.LIMIT,
            bestPrice
        )
    }

    override fun endPosition(position: BinancePosition) {
        binanceTrader.endOrder(position)
    }
}