package com.harmanec.krypto.exchange

interface Trader {
    fun longCoin()
    fun shortCoin()
    fun endPosition(position: BinancePosition)
}