package com.harmanec.krypto.exchange

import com.binance.api.client.domain.OrderStatus
import com.binance.client.RequestOptions
import com.binance.client.impl.BinanceApiInternalFactory
import com.binance.client.model.enums.OrderSide
import com.binance.client.model.enums.OrderType
import com.binance.client.model.trade.Order
import com.harmanec.krypto.Config
import com.harmanec.krypto.logger.FileLogger
import com.njkim.reactivecrypto.binance.model.BinanceOrderStatusType
import java.math.BigDecimal

class BinanceOrder(var order: Order,
                   private var listener: BinanceOrderListener?,
                   private val longStopLoss : Double = 0.988,
                   private val shortStopLoss : Double = 1.02,
                   private val longProfit : Double = 1.1,
                   private val shortProfit : Double = 0.93) : BinanceWSListener {

    private var stopLossOrder : Order? = null
    private var profitOrder : Order? = null
    private var cancelOrder : Order? = null

    private var websockets : BinanceWS? = null

    private val restClient = BinanceApiInternalFactory.getInstance().createSyncRequestClient(
        Config.BinanceAPIKey,
        Config.BinanceSecretKey,
        RequestOptions()
    )

    init {
        val checkOrder = restClient.getOrder(order.symbol, order.orderId, order.clientOrderId)
        if (checkOrder.status == OrderStatus.FILLED.name) {
            if (stopLossOrder == null && profitOrder == null) {
                order = checkOrder
                createOrders()

                FileLogger.log("Order filled at ${checkOrder.price} (quantity: ${checkOrder.executedQty})")
            }
        }

        websockets = BinanceWS(this)
    }

    fun endOrder() {
        order = restClient.getOrder(order.symbol, order.orderId, order.clientOrderId)
        if (order.status == OrderStatus.FILLED.name
            && (stopLossOrder != null || profitOrder != null)
            && stopLossOrder?.status != BinanceOrderStatusType.CANCELED.name
            && profitOrder?.status != BinanceOrderStatusType.CANCELED.name
        ) {
            createCancelOrder()
        } else {
            restClient.cancelOrder(order.symbol, order.orderId, order.clientOrderId)
            cancelOrders()
        }
    }

    private fun createOrders() {
        val stopLossPrice = order.price.times(if (order.side.equals(OrderSide.SELL.name)) shortStopLoss.toBigDecimal() else longStopLoss.toBigDecimal())
        stopLossOrder = Binance.placeOrder(
            "stopLossOrder",
            if (order.side.equals(OrderSide.SELL.name)) OrderSide.BUY else OrderSide.SELL,
            order.origQty,
            OrderType.STOP_MARKET,
            stopLossPrice,
            stopLossPrice
        )

        val profitPrice = order.price.times(if (order.side.equals(OrderSide.SELL.name)) shortProfit.toBigDecimal() else longProfit.toBigDecimal())
        profitOrder = Binance.placeOrder(
            "profitOrder",
            if (order.side.equals(OrderSide.SELL.name)) OrderSide.BUY else OrderSide.SELL,
            order.origQty,
            OrderType.TAKE_PROFIT_MARKET,
            profitPrice,
            profitPrice
        )

        FileLogger.log("Setting stop-loss at ${stopLossOrder?.stopPrice} and profit at ${profitOrder?.stopPrice}")
    }

    private fun createCancelOrder() {
        cancelOrder = Binance.placeOrder(
            "${order.clientOrderId}rev",
            if(order.side.equals(OrderSide.SELL.name)) OrderSide.BUY else OrderSide.SELL,
            order.origQty,
            OrderType.MARKET,
            null,
            null
        )

        FileLogger.log("Ending early. Cancel order created")
    }

    private fun cancelOrders() {
        try {
            restClient.cancelAllOpenOrder(order.symbol)
        } catch (e: java.lang.Exception) {
            FileLogger.log("Cancelling orders error: ${e.message}")
        }

        FileLogger.log("Cancelling all open orders.")
    }

    override fun orderChange(order: Order) {
        if ((order.status == OrderStatus.FILLED.name
                    || order.status == OrderStatus.EXPIRED.name
                    || order.status == OrderStatus.NEW.name
                    || order.status == OrderStatus.CANCELED.name)
            && order.executedQty > BigDecimal.ZERO) {
            when (order.clientOrderId) {
                this.order.clientOrderId -> {
                    this.order = order
                    createOrders()
                }
                stopLossOrder?.clientOrderId,
                profitOrder?.clientOrderId,
                cancelOrder?.clientOrderId -> {
                    listener?.positionEnd()
                    websockets?.closeWebsockets()
                    websockets = null
                    listener = null
                    cancelOrders()
                }
            }
        }

        FileLogger.log("Order changed: $order")
    }
}