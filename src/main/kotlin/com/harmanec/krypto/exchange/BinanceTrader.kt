package com.harmanec.krypto.exchange

import com.binance.client.model.enums.OrderSide
import com.binance.client.model.enums.OrderType
import com.harmanec.krypto.logger.FileLogger
import java.math.BigDecimal

class BinanceTrader : BinanceOrderListener {
    private val position : BinancePosition
        get() = when(order?.order?.side) {
            OrderSide.SELL.name -> BinancePosition.SHORT
            OrderSide.BUY.name -> BinancePosition.LONG
            else -> BinancePosition.NONE
        }

    private var currOrder : Long = 1
    private var order : BinanceOrder? = null

    fun placeOrder(side: OrderSide, quantity: BigDecimal, orderType: OrderType, price: BigDecimal) {
        if (position == BinancePosition.NONE) {
            val placedOrder = Binance.placeOrder("order${currOrder}", side, quantity, orderType, price, null)
            if (placedOrder != null) {
                order = BinanceOrder(placedOrder, this)
            }
            currOrder++
        } else {
            FileLogger.log("Trying to enter $side position at $price but alredy in a position: $position")
        }
    }

    fun endOrder(orderPosition: BinancePosition) {
        if (position == orderPosition && order != null) {
            order?.endOrder()
        } else {
            FileLogger.log("Trying to end early but not in correct position (ending: $orderPosition / in: $position)")
        }
    }

    override fun positionEnd() {
        order = null

        FileLogger.log("Position ended. Current position: $position")
    }
}