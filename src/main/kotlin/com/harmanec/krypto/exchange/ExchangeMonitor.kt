package com.harmanec.krypto.exchange

import com.harmanec.krypto.logger.FileLogger
import com.njkim.reactivecrypto.core.ExchangeClientFactory
import com.njkim.reactivecrypto.core.common.model.ExchangeVendor
import com.njkim.reactivecrypto.core.common.model.currency.Currency
import com.njkim.reactivecrypto.core.common.model.currency.CurrencyPair
import com.njkim.reactivecrypto.core.common.model.order.OrderBookUnit
import reactor.core.Disposable
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * Monitoring of open bids and asks on exchange
 * @param currency coin
 */
class ExchangeMonitor(private val currency: Currency) {
    private val websocketClient = ExchangeClientFactory.publicWebsocket(ExchangeVendor.BINANCE)
    private val bids: MutableList<OrderBookUnit> = Collections.synchronizedList(ArrayList())
    private val asks: MutableList<OrderBookUnit> = Collections.synchronizedList(ArrayList())
    private var runner : Disposable
    private var lastUpdate: Long = System.currentTimeMillis()

    init {
        runner = createWebsockets()

        val service = Executors.newSingleThreadScheduledExecutor()
        service.scheduleAtFixedRate({
            if ((lastUpdate + 60000) < System.currentTimeMillis()) {
                restart()
            }
        }, 1, 1, TimeUnit.MINUTES)
    }

    /**
     * Restarts websockets.
     * Should be called when disconnected
     */
    private fun restart() {
        runner.dispose()
        runner = createWebsockets()
        FileLogger.log("Resetting exchange monitor after 1m inactivity.")
    }

    private fun createWebsockets() : Disposable {
        return websocketClient.createDepthSnapshot(listOf(CurrencyPair(currency, Currency.USDT)))
            .doOnNext { orderBook ->
                synchronized(bids) {
                    bids.clear()
                    bids.addAll(orderBook.bids)
                }
                synchronized(asks) {
                    asks.clear()
                    asks.addAll(orderBook.asks)
                }
                lastUpdate = System.currentTimeMillis()
            }
            .doOnError { error ->
                println(error.message)
            }
            .subscribe()
    }

    /**
     * Get price at which the coin should be sold
     * @param budget amount of the coin
     * @return price
     */
    fun getBestSellingPrice(budget: BigDecimal) : BigDecimal {
        var amount = budget
        synchronized(bids) {
            if (bids.size > 0) {
                bids.sortedBy { it.price }
                    .forEach {
                        amount -= it.quantity * it.price
                        if (amount <= BigDecimal.ZERO) return it.price
                    }

                return bids[bids.size - 1].price
            }

            return BigDecimal(1000000)
        }
    }

    /**
     * Get price at which should the coin be bought
     * @param budget
     * @return price
     */
    fun getBestBuyingPrice(budget: BigDecimal) : BigDecimal {
        var coinAmount = budget
        synchronized(asks) {
            if (asks.size > 0) {
                asks.sortedByDescending { it.price }
                    .forEach {
                        coinAmount -= it.quantity
                        if (coinAmount <= BigDecimal.ZERO) return it.price
                    }

                return asks[asks.size - 1].price
            }

            return BigDecimal.ZERO
        }
    }
}