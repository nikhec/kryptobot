package com.harmanec.krypto.exchange

import com.harmanec.krypto.logger.FileLogger
import com.harmanec.krypto.model.Ticker
import com.njkim.reactivecrypto.core.ExchangeClientFactory
import com.njkim.reactivecrypto.core.common.model.ExchangeVendor
import com.njkim.reactivecrypto.core.common.model.currency.Currency
import com.njkim.reactivecrypto.core.common.model.currency.CurrencyPair
import reactor.core.Disposable
import java.math.BigDecimal
import java.time.ZoneId
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * Keeps track of coin price through fulfilled orders
 * @param currency coin
 */
class PriceChecker(private val currency: Currency) {
    private val websocketClient = ExchangeClientFactory.publicWebsocket(ExchangeVendor.BINANCE)
    private val listeners: MutableList<PriceListener> = ArrayList()
    private val current: MutableList<BigDecimal> = ArrayList()
    private val currentVolume: MutableList<BigDecimal> = ArrayList()
    private val currentTimestamps: MutableList<Long> = ArrayList()
    private val previous: MutableList<BigDecimal> = ArrayList()
    private val previousVolume: MutableList<BigDecimal> = ArrayList()
    private val previousTimestamps: MutableList<Long> = ArrayList()
    private var runner : Disposable

    private var lastEmpty = 0

    init {
        runner = createWebsockets()

        val service: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
        val priceCalculator = Runnable {
            calculatePrice()
        }

        service.scheduleAtFixedRate(priceCalculator, 0, 1000, TimeUnit.MILLISECONDS)
    }

    private fun restart() {
        runner.dispose()
        runner = createWebsockets()
    }

    private fun createWebsockets() : Disposable {
        return websocketClient.createTradeWebsocket(listOf(CurrencyPair(currency, Currency.USDT)))
            .doOnNext { tick ->
                synchronized(current) {
                    current.add(tick.price)
                    currentVolume.add(tick.quantity)
                    currentTimestamps.add(tick.eventTime.toLocalDateTime().atZone(ZoneId.systemDefault()).toEpochSecond())
                }
            }
            .doOnError { error ->
                println(error.localizedMessage)
            }
            .subscribe()
    }

    /**
     * Add price change listener
     * @param listener
     */
    fun addListener(listener: PriceListener) {
        listeners.add(listener)
    }

    private fun calculatePrice() {
        synchronized(current) {
            previous.addAll(current)
            current.clear()
            previousVolume.addAll(currentVolume)
            currentVolume.clear()
            previousTimestamps.addAll(currentTimestamps)
            currentTimestamps.clear()

            if (previous.isNotEmpty()) {
                val ticker = Ticker(
                    previous.first(),
                    previous.minOrNull() ?: BigDecimal.ZERO,
                    previous.maxOrNull() ?: BigDecimal.ZERO,
                    previous.last(),
                    previousVolume.sum(),
                    previous.size,
                    previousTimestamps.last()
                )
                listeners.forEach { listener ->
                    listener.priceChange(currency, ticker)
                }

                previous.clear()
                previousVolume.clear()
                lastEmpty = 0
            } else {
                lastEmpty++
                if (lastEmpty >= 60) {
                    lastEmpty = 0
                    restart()

                    FileLogger.log("Resetting price checker after 1m inactive")
                }
            }
        }
    }
}

fun MutableList<BigDecimal>.sum(): BigDecimal {
    var sum = BigDecimal.ZERO
    this.forEach { it ->
        sum += it
    }

    return sum
}

operator fun BigDecimal.div(count: Int): BigDecimal {
    return this / BigDecimal(count)
}
