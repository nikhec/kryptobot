package com.harmanec.krypto.exchange

interface BinanceOrderListener {
    /**
     * Is called when position ends
     */
    fun positionEnd()
}