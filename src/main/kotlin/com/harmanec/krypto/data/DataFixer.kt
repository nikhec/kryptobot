package com.harmanec.krypto.data

import java.math.BigDecimal
import java.math.MathContext
import kotlin.math.roundToInt

data class PricesDataRow(val openTime: Long, val open: BigDecimal, val high: BigDecimal, val low: BigDecimal, val close: BigDecimal, val volume: BigDecimal, val closeTime: Long, val assetVolume: BigDecimal, val trades: Int, val buyBaseAssetVolume: BigDecimal, val buyQuoteAssetVolume: BigDecimal, val ignore: Int)
/**
 * BINANCE data structure
 * 1499040000000,      // Open time
 * "0.01634790",       // Open
 * "0.80000000",       // High
 * "0.01575800",       // Low
 * "0.01577100",       // Close
 * "148976.11427815",  // Volume
 * 1499644799999,      // Close time
 * "2434.19055334",    // Quote asset volume
 * 308,                // Number of trades
 * "1756.87402397",    // Taker buy base asset volume
 * "28.46694368",      // Taker buy quote asset volume
 * "17928899.62484339" // Ignore.
 */
class DataFixer {
    companion object {
        fun completeData(start: PricesDataRow, end: PricesDataRow) : List<List<String>> {
            val completedData = ArrayList<List<String>>()

            val steps = (end.openTime - start.openTime) / 60000
            val openAdd = (end.open - start.open) / steps
            val highAdd = (end.high - start.high) / steps
            val lowAdd = (end.low - start.low) / steps
            val volumeAdd = (end.volume - start.volume) / steps
            val closeAdd = (end.close - start.close) / steps
            val assetAdd = (end.assetVolume - start.assetVolume) / steps
            val tradesAdd = ((end.trades - start.trades) / steps).toDouble().roundToInt()
            val buyBaseAdd = (end.buyBaseAssetVolume - start.buyBaseAssetVolume) / steps
            val buyQuoteAdd = (end.buyQuoteAssetVolume - start.buyQuoteAssetVolume) / steps

            var startTime = start.openTime + 60000
            var openStart = start.open
            var highStart = start.high
            var lowStart = start.low
            var closeStart = start.close
            var volumeStart = start.volume
            var assetStart = start.assetVolume
            var tradesStart = start.trades
            var buyBaseStart = start.buyBaseAssetVolume
            var buyQuoteStart = start.buyQuoteAssetVolume
            while (startTime < end.openTime) {
                openStart += openAdd
                highStart += highAdd
                lowStart += lowAdd
                closeStart += closeAdd
                volumeStart += volumeAdd
                assetStart += assetAdd
                tradesStart += tradesAdd
                buyBaseStart += buyBaseAdd
                buyQuoteStart += buyQuoteAdd

                val data = listOf("$startTime","$openStart","$highStart","$lowStart","$closeStart","$volumeStart","${startTime + 59999}","$assetStart","$tradesStart","$buyBaseStart","$buyQuoteStart","0")
                completedData.add(data)

                startTime += 60000
            }

            return completedData
        }
    }
}

private operator fun BigDecimal.div(steps: Long): BigDecimal {
    return this.divide(BigDecimal(steps), MathContext.DECIMAL128)
}
