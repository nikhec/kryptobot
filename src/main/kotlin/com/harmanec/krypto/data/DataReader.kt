package com.harmanec.krypto.data

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.utils.Utils
import java.math.BigDecimal
import java.util.*
import kotlin.collections.ArrayList

/**
 * BINANCE data structure
 * 1499040000000,      // Open time
 * "0.01634790",       // Open
 * "0.80000000",       // High
 * "0.01575800",       // Low
 * "0.01577100",       // Close
 * "148976.11427815",  // Volume
 * 1499644799999,      // Close time
 * "2434.19055334",    // Quote asset volume
 * 308,                // Number of trades
 * "1756.87402397",    // Taker buy base asset volume
 * "28.46694368",      // Taker buy quote asset volume
 * "17928899.62484339" // Ignore.
 */
class DataReader(private val fileUrl: String) {
    private val minutelyTickers = ArrayList<Ticker>()

    private var startDate : Long = 0L

    init {
        prepareData()
    }

    /**
     * Loads data from file for later usage
     */
    private fun prepareData() {
        var i = 1
        var lastPrice: PricesDataRow? = null
        csvReader().open(fileUrl) {
            readAllAsSequence().forEach { row ->
                val currentPrice = PricesDataRow(row[0].toLong(), row[1].toBigDecimal(), row[2].toBigDecimal(), row[3].toBigDecimal(), row[4].toBigDecimal(), row[5].toBigDecimal(), row[6].toLong(), row[7].toBigDecimal(), row[8].toInt(), row[9].toBigDecimal(), row[10].toBigDecimal(), 0)
                if (lastPrice != null &&
                    row[0].toLong() - lastPrice!!.openTime > 60000) {
                    DataFixer.completeData(lastPrice!!, currentPrice).forEach { row ->
                        addMinutelyData(row, i)
                        i++
                    }
                }
                addMinutelyData(row, i)
                lastPrice = currentPrice

                if (i == 1) {
                    startDate = row[0].toLong()
                }
                i++
            }
        }
        println("minutely: ${minutelyTickers.size}")
    }

    private fun addMinutelyData(row: List<String>, i: Int) {
        minutelyTickers.add(
            Ticker(
                row[1].toBigDecimal(),
                row[3].toBigDecimal(),
                row[2].toBigDecimal(),
                row[4].toBigDecimal(),
                row[5].toBigDecimal(),
                row[8].toInt(),
                row[6].toLong()
            )
        )
    }

    fun getMinutelyTickers(minutes: Int) : List<Ticker> {
        return minutelyTickers.chunked(minutes).map {
            Utils.aggregateTickers(it)
        }
    }

    /**
     * Get minutely data
     * @param dataPrice data type
     * @param minutes data every x minutes
     */
    fun getMinutelyData(dataPrice: DataPrice, minutes: Int) : List<BigDecimal> {
        return minutelyTickers.chunked(minutes).map {
            val ticker = Utils.aggregateTickers(it)
            when(dataPrice) {
                DataPrice.OPEN -> ticker.open
                DataPrice.HIGH -> ticker.high
                DataPrice.LOW -> ticker.low
                DataPrice.CLOSE -> ticker.close
                DataPrice.VOLUME -> ticker.volume
                DataPrice.TRADES -> ticker.trades.toBigDecimal()
            }
        }
    }

    fun getHourlyTickers(hours: Int) : List<Ticker> {
        return minutelyTickers.chunked(60 * hours).map {
            Utils.aggregateTickers(it)
        }
    }

    /**
     * Get hourly data
     * @param dataPrice data type
     * @param hours data every x hours
     */
    fun getHourlyData(dataPrice: DataPrice, hours: Int) : List<BigDecimal> {
        return minutelyTickers.chunked(60 * hours).map {
            val ticker = Utils.aggregateTickers(it)
            when(dataPrice) {
                DataPrice.OPEN -> ticker.open
                DataPrice.HIGH -> ticker.high
                DataPrice.LOW -> ticker.low
                DataPrice.CLOSE -> ticker.close
                DataPrice.VOLUME -> ticker.volume
                DataPrice.TRADES -> ticker.trades.toBigDecimal()
            }
        }
    }

    fun getDailyTickers(days: Int) : List<Ticker> {
        return minutelyTickers.chunked(24 * 60 * days).map {
            Utils.aggregateTickers(it)
        }
    }

    /**
     * Get daily data
     * @param dataPrice data type
     * @param days data every x days
     */
    fun getDailyData(dataPrice: DataPrice, days: Int) : List<BigDecimal> {
        return minutelyTickers.chunked(24 * 60 * days).map {
            val ticker = Utils.aggregateTickers(it)
            when(dataPrice) {
                DataPrice.OPEN -> ticker.open
                DataPrice.HIGH -> ticker.high
                DataPrice.LOW -> ticker.low
                DataPrice.CLOSE -> ticker.close
                DataPrice.VOLUME -> ticker.volume
                DataPrice.TRADES -> ticker.trades.toBigDecimal()
            }
        }
    }

    /**
     * Get start date of the data
     */
    fun getStartDate() : Long = startDate

}

enum class DataPrice(val index: Int) {
    OPEN(1),
    HIGH(2),
    LOW(3),
    CLOSE(4),
    VOLUME(5),
    TRADES(8)
}