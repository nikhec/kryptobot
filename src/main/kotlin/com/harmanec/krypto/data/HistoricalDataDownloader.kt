package com.harmanec.krypto.data

import com.binance.api.client.BinanceApiClientFactory
import com.binance.api.client.domain.market.Candlestick
import com.binance.api.client.domain.market.CandlestickInterval
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import com.harmanec.krypto.Config
import com.harmanec.krypto.model.Ticker
import net.lingala.zip4j.ZipFile
import java.io.File
import java.io.FileOutputStream
import java.io.FileWriter
import java.net.URL
import java.nio.channels.Channels
import java.nio.channels.ReadableByteChannel
import java.util.*
import kotlin.collections.ArrayList
import java.lang.Integer.min as min


class HistoricalDataDownloader {
    companion object {
        /**
         * Downloads 1m price data from Binance. Stores it in data/$coin/prices.csv and daily data in data/$coin/daily/$coin_1m-year-month-day.csv
         * @param coin coin pair (example: BTCUSDT)
         * @param months index buy month number, gives start and end day between which the data should be downloaded (inclusive)
         * (example: months[9] = Pair(3,5) means that for september will be downloaded data from 3rd to 5th day)
         * @param startMonth start month of the data
         * @param endMonth end month of the data
         */
        fun downloadData(coin: String, startDate: Date, endDate: Date) {
            println("Getting data for: $coin")
            val calendar = Calendar.getInstance()
            calendar.time = startDate
            while (calendar.time.before(endDate)) {
                File("data/$coin/daily").mkdirs()

                val dataUrl = "https://data.binance.vision/data/spot/daily/klines/$coin/1m/$coin-1m-2021-${String.format("%02d", calendar.get(Calendar.MONTH) + 1)}-${String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))}.zip"
                println("Downloading: $dataUrl")
                val website = URL(dataUrl)
                val rbc: ReadableByteChannel = Channels.newChannel(website.openStream())

                val zipName = "data/$coin/daily/${coin}-1m-2021-${String.format("%02d", calendar.get(Calendar.MONTH) + 1)}-${String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))}.zip"
                val fos = FileOutputStream(zipName)
                fos.channel.transferFrom(rbc, 0, Long.MAX_VALUE)

                val zipFile = ZipFile(zipName)
                zipFile.extractAll("data/$coin/daily")
                zipFile.close()

                File(zipName).delete()

                val file = File("data/$coin/prices.csv")
                file.createNewFile()

                val writer = FileWriter("data/$coin/prices.csv", true)
                csvReader().open(zipName.replace(".zip", ".csv")) {
                    readAllAsSequence().forEach { row ->
                        for (j in 0..10) {
                            writer.append(row[j])
                            writer.append(",")
                        }
                        writer.append(row[11])
                        writer.append("\n")
                    }
                }
                writer.flush()
                writer.close()

                calendar.add(Calendar.DAY_OF_MONTH, 1)
            }
        }

        /**
         * Get last date that data is downloaded for given coin. If no data downloaded returns -6 months from now
         * @param coin coin pair
         * @return date
         */
        fun lastDateDownloaded(coin: String): Date {
            val dir = File("data/$coin/daily")

            val calendar = Calendar.getInstance()
            if (dir.isDirectory) {
                val opFile: Optional<File> = Arrays.stream(dir.listFiles { obj: File -> obj.isFile })
                    .max { f1, f2 -> java.lang.Long.compare(f1.lastModified(), f2.lastModified()) }

                if (opFile.isPresent) {
                    val date = opFile.get().name.replace("$coin-1m-", "")
                        .replace(".csv", "")
                    val split = date.split("-")
                    calendar.set(split[0].toInt(), split[1].toInt(), split[2].toInt())
                } else {
                    calendar.add(Calendar.MONTH, -6)
                }
            } else {
                calendar.add(Calendar.MONTH, -6)
            }

            return calendar.time
        }

        /**
         * Gets last downloaded data for given coin and download newer ones till yesterday
         * @param coin coin pair
         */
        fun downloadRecentData(coin: String) {
            downloadData(coin, lastDateDownloaded(coin), Date())
        }

        fun getLastHistoricCandlestick(coin: String, interval: CandlestickInterval, count: Int) : List<Ticker> {
            val factory = BinanceApiClientFactory.newInstance(Config.BinanceAPIKey, Config.BinanceSecretKey)
            val client = factory.newRestClient()

            val tickers = ArrayList<Ticker>()

            val calendar = Calendar.getInstance()
            val nowTime = calendar.timeInMillis
            var startDate = nowTime - (count * 60000)
            var endDate = kotlin.math.min((startDate + 1000*60000), nowTime)
            var fetched = 0
            while (fetched < count) {
                val number = min(1000, (count - fetched))

                val candlesticks = client.getCandlestickBars(coin, interval, number, startDate, endDate)
                candlesticks.forEach {
                    tickers.add(candlestickToTicker(it))
                }

                fetched += number
                startDate += 60000*1000
                endDate += 60000*1000
            }

            return tickers
        }
    }
}

fun candlestickToTicker(candlestick: Candlestick) : Ticker {
    return Ticker(
        candlestick.open.toBigDecimal(),
        candlestick.low.toBigDecimal(),
        candlestick.high.toBigDecimal(),
        candlestick.close.toBigDecimal(),
        candlestick.volume.toBigDecimal(),
        candlestick.numberOfTrades.toInt(),
        candlestick.closeTime
    )
}