package com.harmanec.krypto.decider

import com.harmanec.krypto.exchange.Trader
import com.harmanec.krypto.model.Ticker

interface Decider {
    var shouldLog: Boolean
    /**
     * Notifies when new price is known
     * @param ticker current ticker
     * @param trader trader for placing orders
     */
    fun priceChange(ticker: Ticker, trader: Trader)

    /**
     * Prints indicator configuration
     */
    fun getConfigurationDescription() : String {
        return ""
    }
}