package com.harmanec.krypto.decider

import com.harmanec.krypto.calculator.StochasticOscillatorCalculator
import com.harmanec.krypto.exchange.*
import com.harmanec.krypto.indicator.calculator.MovingAverageCalculator
import com.harmanec.krypto.logger.FileLogger
import com.harmanec.krypto.model.Ticker
import com.njkim.reactivecrypto.core.common.model.currency.Currency
import java.math.BigDecimal

class DeciderImpl(data: List<Ticker>, aggregator: PriceAggregator) : AggregatedPriceListener {
    private val trader : FutureTrader
    private val exchangeMonitor = ExchangeMonitor(Currency.BTC)

    private val indicatorSmall = MovingAverageCalculator(data.map { it.close }, 2)
    private val indicatorBig = MovingAverageCalculator(data.map { it.close }, 10)
    private val stochasticOscillatorCalculator = StochasticOscillatorCalculator(data.map {it.close}, data.map {it.low}, data.map { it.high }, 2)

    private var lastSmallValue: BigDecimal = BigDecimal.ZERO
    private var lastBigValue: BigDecimal = BigDecimal.ZERO

    init {
         trader = FutureTrader(exchangeMonitor)

        aggregator.addListener(this, TimeInterval.HOUR, 8)
    }

    override fun priceChange(currency: Currency, price: Ticker) {
        val valueSmall = indicatorSmall.addPrice(price.close)
        val valueBig = indicatorBig.addPrice(price.close)
        val so = stochasticOscillatorCalculator.addPrice(price.close, price.low, price.high)

        if ((lastSmallValue < lastBigValue) && (valueSmall > valueBig)) {
            trader.endPosition(BinancePosition.SHORT)
            if (so > BigDecimal(10)) {
                trader.longCoin()
            }
        } else if ((lastSmallValue > lastBigValue) && (valueSmall < valueBig)) {
            trader.endPosition(BinancePosition.LONG)
            if (so < BigDecimal(90)) {
                trader.shortCoin()
            }
        }

        lastSmallValue = valueSmall
        lastBigValue = valueBig

        FileLogger.log(price.toString())
    }
}