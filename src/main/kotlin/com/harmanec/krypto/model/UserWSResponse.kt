package com.harmanec.krypto.model

import com.binance.client.model.trade.Order
import kotlinx.serialization.Serializable

@Serializable
data class UserWSResponse(
    val e: String,
    val E: Long,
    val T: Long?,

    // Margin call
    val cw: String?,
    val p: List<MarginCall>?,

    // Account update
    val a: AccountUpdate?,

    // Order update
    val o: OrderUpdate?,

    // Account configuration
    val ac: AccountConfiguration?,
    val ai: AccountConfig
)

@Serializable
data class MarginCall (
    val s: String,
    val ps: String,
    val pa: String,
    val mt: String,
    val iw: String,
    val mp: String,
    val up: String,
    val mm: String
)

@Serializable
data class AccountUpdate(
    val m: String,
    val B: List<AccountUpdateBalance>?,
    val P: List<AccountUpdatePosition>?
)

@Serializable
data class AccountUpdateBalance(
    val a: String,
    val wb: String,
    val cw: String,
    val bc: String
)

@Serializable
data class AccountUpdatePosition(
    val s: String,
    val pa: String,
    val ep: String,
    val cr: String,
    val up: String,
    val mt: String,
    val iw: String,
    val ps: String
)

@Serializable
data class OrderUpdate(
    private val s: String,
    val c: String,
    val S: String,
    val o : String,
    val f: String,
    val q: String,
    val p: String,
    val ap: String,
    val sp: String,
    private val x: String,
    val X: String,
    val i: Long,
    private val l: String,
    val z: String,
    val L: String,
    private val N: String,
    val n: String,
    private val T: Long,
    val t: Long,
    val b: String,
    val a: String,
    val m: Boolean,
    val R: Boolean,
    val wt: String,
    val ot: String,
    val ps: String,
    val cp: Boolean,
    val AP: String,
    val cr: String,
    val pP: Boolean,
    val si: Int,
    val ss: Int,
    val rp: String
) {
    fun toOrder(): Order {
        val order = Order()
        order.type = o
        order.orderId = i
        order.price = L.toBigDecimal()
        order.side = S
        order.clientOrderId = c
        order.stopPrice = sp.toBigDecimal()
        order.executedQty = l.toBigDecimal()
        order.positionSide = ps
        order.origQty = q.toBigDecimal()
        order.reduceOnly = R
        order.status = X
        order.symbol = s
        order.timeInForce = f
        order.workingType = wt

        return order
    }
}

@Serializable
data class AccountConfiguration(
    val s: String,
    val l: Int
)

@Serializable
data class AccountConfig (
    val j: Boolean
)