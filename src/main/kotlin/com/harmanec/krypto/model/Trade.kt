package com.harmanec.krypto.model

import java.math.BigDecimal

data class Trade(val side: TradeSide, val amount: Double, val price: BigDecimal)

enum class TradeSide {
    BUY,
    SELL
}