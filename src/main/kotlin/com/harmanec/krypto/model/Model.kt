package com.harmanec.krypto.model

import java.math.BigDecimal

data class Ticker(val open: BigDecimal,
                   val low: BigDecimal,
                   val high: BigDecimal,
                   val close: BigDecimal,
                   val volume: BigDecimal,
                   val trades: Int,
                   val timestamp: Long)