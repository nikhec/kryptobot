@file:JvmName("KryploBot")

package com.harmanec.krypto

import com.binance.api.client.domain.market.CandlestickInterval
import com.harmanec.krypto.data.HistoricalDataDownloader
import com.harmanec.krypto.decider.DeciderImpl
import com.harmanec.krypto.exchange.PriceAggregator
import com.harmanec.krypto.exchange.PriceChecker
import com.harmanec.krypto.logger.FileLogger
import com.njkim.reactivecrypto.core.common.model.currency.Currency

/**
 * Main
 *
 * Run the actual Crypto bot
 *
 * @param args
 */
fun main(args: Array<String>) {
    Main.run()
}

class Main {
    companion object {
        fun run() {
            val tickers = HistoricalDataDownloader.getLastHistoricCandlestick("BTCUSDT", CandlestickInterval.EIGHT_HOURLY, 10)

            val priceChecker = PriceChecker(Currency.BTC)
            val priceAggregator = PriceAggregator(Currency.BTC)
            priceChecker.addListener(priceAggregator)
            val decider = DeciderImpl(tickers, priceAggregator)

            FileLogger.log("Bot started.")
        }
    }
}