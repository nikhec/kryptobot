package com.harmanec.krypto.logger


interface Logger {
    /**
     * Logs given message
     */
    fun log(text: String)
}