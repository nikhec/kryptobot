package com.harmanec.krypto.logger

import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.thread

class FileLogger {
    companion object : Logger {
        private val calendar : Calendar = Calendar.getInstance()
        private val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        override fun log(text: String) {
            thread(start = true) {
                calendar.time = Date()
                val logFileName =
                    "${calendar.get(Calendar.DAY_OF_MONTH)}-${calendar.get(Calendar.MONTH)+1}-${calendar.get(Calendar.YEAR)}-kryplobot.log"
                val file = File("logs/$logFileName")
                file.createNewFile()
                file.appendText("${dateFormatter.format(calendar.time)}: ${text}\n")
            }
        }
    }
}