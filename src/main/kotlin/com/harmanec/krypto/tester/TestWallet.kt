package com.harmanec.krypto.tester

import java.math.BigDecimal

class TestWallet {
    var usdt = BigDecimal(1000)
    var btc = BigDecimal.ZERO

    private var inShort: Boolean = false
    private var shortEnterPrice: BigDecimal = BigDecimal.ZERO

    fun reset() {
        usdt = BigDecimal(1000)
        btc = BigDecimal.ZERO
        inShort = false
        shortEnterPrice = BigDecimal.ZERO
    }

    fun enteredShort(price: BigDecimal) {
        inShort = true
        shortEnterPrice = price
    }

    fun endShort() {
        inShort = false
    }

    fun getResourcesInDollars(btcPrice: BigDecimal): BigDecimal {
        return when(inShort) {
            true -> usdt + ((shortEnterPrice - btcPrice) * btc) + (shortEnterPrice * btc)
            false -> usdt + (btc * btcPrice)
        }
    }
}