package com.harmanec.krypto.tester

import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.model.TradeSide
import java.io.File
import java.math.BigDecimal
import kotlin.concurrent.thread

class TestLogger {
    companion object {
        /**
         * Logs to given file
         *
         * @param fileName name of the log file
         * @param log information that should be logged
         */
        fun logCsv(fileName: String, log: CsvLog) {
            val file = File("logs/tester/$fileName")
            file.createNewFile()
            file.appendText("${log.toCsv()}\n")
        }

        /**
         * Logs to given file
         *
         * @param fileName name of the log file
         * @param log information that should be logged
         */
        fun logText(fileName: String, log: String) {
            val file = File("logs/tester/$fileName")
            file.createNewFile()
            file.appendText("${log}\n")
        }

        /**
         * Deletes log with given name
         *
         * @param fileName name of the log file
         */
        fun deleteLog(fileName: String) {
            thread(start = true) {
                val file = File("logs/tester/$fileName")
                file.delete()
            }
        }
    }
}

data class TradeInfo(
    val date: Long,
    val side: TradeSide,
    val amount: BigDecimal,
    val price: BigDecimal,
    val profit: BigDecimal
) : CsvLog {
    override fun toCsv() : String {
        return "$date,$side,$amount,$price,$profit"
    }
}

data class TickerValue(
    val ticker: Ticker
) : CsvLog {
    override fun toCsv(): String {
        return "${ticker.timestamp},${ticker.open},${ticker.high},${ticker.low},${ticker.close}"
    }
}

data class WalletBudget(
    val timestamp: Long,
    val budget: BigDecimal
) : CsvLog {
    override fun toCsv(): String {
        return "$timestamp,$budget"
    }
}

data class WMACrossIndicatorValue(
    val timestamp: Long,
    val low: BigDecimal,
    val high: BigDecimal
) : CsvLog {
    override fun toCsv(): String {
        return "$timestamp,$low,$high"
    }
}

data class IndicatorValue(
    val date: Long,
    val value: BigDecimal,
    val additional: String = ""
) : CsvLog {
    override fun toCsv(): String {
        return "$date,$value,$additional"
    }
}

interface CsvLog {
    fun toCsv() : String
}