package com.harmanec.krypto.tester

import com.harmanec.krypto.exchange.BinancePosition
import com.harmanec.krypto.exchange.Trader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.model.TradeSide
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*

class TestTrader(
    val longProfit: Double,
    val longStoploss: Double,
    val shortProfit: Double,
    val shortStoploss: Double,
    val fee: Double,
    val wallet: TestWallet,
    val logFile: String?) : Trader {

    private lateinit var currentPrice: Ticker

    var currentPosition: BinancePosition = BinancePosition.NONE
    private var enterPrice: Ticker? = null
    private var enterBudget: BigDecimal = BigDecimal.ZERO

    fun priceChange(ticker: Ticker) {
        currentPrice = ticker
        when(currentPosition) {
            BinancePosition.LONG -> {
                enterPrice?.let { enterPrice ->
                    if (enterPrice.close * longProfit.toBigDecimal() <= currentPrice.close // profit
                        || enterPrice.close * longStoploss.toBigDecimal() >= currentPrice.close) { // stoploss
                        endLong()
                    }
                }
            }
            BinancePosition.SHORT -> {
                enterPrice?.let { enterPrice ->
                    if (enterPrice.close * shortProfit.toBigDecimal() >= currentPrice.close // profit
                        || enterPrice.close * shortStoploss.toBigDecimal() <= currentPrice.close) { // stoploss
                        endShort()
                    }
                }
            }
            else -> {}
        }
    }

    override fun longCoin() {
        if (currentPosition == BinancePosition.NONE) {
            logFile?.let {
                TestLogger.logCsv(logFile, TradeInfo(
                    currentPrice.timestamp,
                    TradeSide.BUY,
                    wallet.usdt.divide(currentPrice.close, MathContext.DECIMAL128),
                    currentPrice.close,
                    BigDecimal.ZERO
                ))

                println(
                    "LONG (${dateFormatter.format(Date(currentPrice.timestamp))}): Buying ${
                        wallet.usdt.divide(
                            currentPrice.close,
                            MathContext.DECIMAL128
                        )
                    }btc for: $${currentPrice.close}"
                )
            }
            startPosition(BinancePosition.LONG)
        }
    }

    override fun shortCoin() {
        if (currentPosition == BinancePosition.NONE) {
            logFile?.let {
                TestLogger.logCsv(logFile, TradeInfo(
                    currentPrice.timestamp,
                    TradeSide.SELL,
                    wallet.usdt.divide(currentPrice.close, MathContext.DECIMAL128),
                    currentPrice.close,
                    BigDecimal.ZERO
                ))
                println(
                    "SHORT (${dateFormatter.format(Date(currentPrice.timestamp))}): Selling ${
                        wallet.usdt.divide(
                            currentPrice.close,
                            MathContext.DECIMAL128
                        )
                    }btc for: $${currentPrice.close}"
                )
            }
            wallet.enteredShort(currentPrice.close)
            startPosition(BinancePosition.SHORT)
        }
    }

    override fun endPosition(position: BinancePosition) {
        if (position == currentPosition) {
            when(currentPosition) {
                BinancePosition.LONG -> endLong()
                BinancePosition.SHORT -> endShort()
                else -> {}
            }
        }
    }

    private fun startPosition(position: BinancePosition) {
        currentPosition = position
        enterPrice = currentPrice
        enterBudget = wallet.usdt
        wallet.btc = wallet.usdt.divide(currentPrice.close, MathContext.DECIMAL128)
        wallet.usdt = BigDecimal.ZERO
    }

    private fun endLong() {
        enterPrice?.let { enterPrice ->
            logFile?.let {
                TestLogger.logCsv(
                    logFile, TradeInfo(
                        currentPrice.timestamp,
                        TradeSide.BUY,
                        wallet.btc,
                        currentPrice.close,
                        (wallet.btc * currentPrice.close) - (wallet.btc * enterPrice.close)
                    )
                )

                println("Ending LONG (${dateFormatter.format(Date(currentPrice.timestamp))}) - profit: $${(wallet.btc * currentPrice.close) - (wallet.btc * enterPrice.close)}")
                println("Current budget: $${wallet.getResourcesInDollars(currentPrice.close).setScale(5, RoundingMode.HALF_UP)}")
            }

            wallet.usdt = wallet.btc * currentPrice.close * fee.toBigDecimal()
            wallet.btc = BigDecimal.ZERO

            currentPosition = BinancePosition.NONE
        }
    }

    private fun endShort() {
        enterPrice?.let {enterPrice ->
            logFile?.let {
                TestLogger.logCsv(logFile, TradeInfo(
                    currentPrice.timestamp,
                    TradeSide.SELL,
                    wallet.btc,
                    currentPrice.close,
                    (wallet.btc * enterPrice.close) - (wallet.btc * currentPrice.close)
                ))

                println("Ending SHORT (${dateFormatter.format(Date(currentPrice.timestamp))}) - profit: $${(wallet.btc * enterPrice.close) - (wallet.btc * currentPrice.close)}")
                println("Current budget: $${wallet.getResourcesInDollars(currentPrice.close)}")
            }
            wallet.usdt = (enterBudget + (wallet.btc * (enterPrice.close - currentPrice.close))) * fee.toBigDecimal()
            wallet.btc = BigDecimal.ZERO
            wallet.endShort()

            currentPosition = BinancePosition.NONE
        }
    }

    companion object {
        val dateFormatter = SimpleDateFormat("dd.MM.yyyy HH:mm")
    }
}