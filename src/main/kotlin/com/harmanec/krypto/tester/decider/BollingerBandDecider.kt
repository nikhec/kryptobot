package com.harmanec.krypto.tester.decider

import com.harmanec.krypto.calculator.BollingerBandsCalculator
import com.harmanec.krypto.decider.Decider
import com.harmanec.krypto.exchange.BinancePosition
import com.harmanec.krypto.exchange.Trader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.TestLogger
import java.math.BigDecimal

class BollingerBandDecider(tickers: List<Ticker>,
                           val step: Int,
                           val multiplicator: Int) : Decider {
    val calculator: BollingerBandsCalculator = BollingerBandsCalculator(tickers.map { it.close }, step, multiplicator)
    var crossedUp: Boolean = true
    var crossedDown: Boolean = true
    var lastPriceValue: BigDecimal = BigDecimal.ZERO

    override var shouldLog: Boolean = false

    override fun priceChange(ticker: Ticker, trader: Trader) {
        val value = calculator.addPrice(ticker.close)
        if (shouldLog) {
            TestLogger.logText("bb/indicator-value.csv", "${ticker.timestamp},${value.movingAverage},${value.lowerBand},${value.upperBand}")
        }

        if (lastPriceValue != BigDecimal.ZERO) {
            if (lastPriceValue <= value.movingAverage.toBigDecimal() && ticker.close >= value.movingAverage.toBigDecimal()) {
                crossedUp = true
                crossedDown = false
                trader.endPosition(BinancePosition.SHORT)
            } else if (lastPriceValue >= value.movingAverage.toBigDecimal() && ticker.close <= value.movingAverage.toBigDecimal()) {
                crossedDown = true
                crossedUp = false
                trader.endPosition(BinancePosition.LONG)
            }
        }

        if (value.lowerBand.toBigDecimal() >= ticker.close
                && crossedDown) {
            trader.endPosition(BinancePosition.SHORT)
            trader.longCoin()
        } else if (value.upperBand.toBigDecimal() <= ticker.close
                && crossedUp) {
            trader.endPosition(BinancePosition.LONG)
            trader.shortCoin()
        }

        lastPriceValue = ticker.close
    }

    override fun getConfigurationDescription(): String {
        return "Step: $step, multiplicator: $multiplicator"
    }
}