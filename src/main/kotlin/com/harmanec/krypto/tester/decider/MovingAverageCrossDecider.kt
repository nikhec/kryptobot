package com.harmanec.krypto.tester.decider

import com.harmanec.krypto.calculator.StochasticOscillatorCalculator
import com.harmanec.krypto.decider.Decider
import com.harmanec.krypto.exchange.BinancePosition
import com.harmanec.krypto.exchange.Trader
import com.harmanec.krypto.indicator.calculator.MovingAverageCalculator
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.TestLogger
import java.math.BigDecimal

class MovingAverageCrossDecider(prices: List<Ticker>,
                                val stepSmall: Int,
                                val stepBig: Int) : Decider {
    private val indicatorSmall = MovingAverageCalculator(prices.map{it.close}, stepSmall)
    private val indicatorBig = MovingAverageCalculator(prices.map{it.close}, stepBig)

    private var lastSmallValue: BigDecimal = BigDecimal.ZERO
    private var lastBigValue: BigDecimal = BigDecimal.ZERO

    override var shouldLog: Boolean = false

    override fun priceChange(ticker: Ticker, trader: Trader) {
        val valueSmall = indicatorSmall.addPrice(ticker.close)
        val valueBig = indicatorBig.addPrice(ticker.close)
        if (shouldLog) {
            TestLogger.logText("maCross/indicator-value.csv", "${ticker.timestamp},$valueSmall,$valueBig")
        }

        if ((lastSmallValue < lastBigValue) && (valueSmall > valueBig)) {
            trader.endPosition(BinancePosition.SHORT)
            trader.longCoin()
        } else if ((lastSmallValue > lastBigValue) && (valueSmall < valueBig)) {
            trader.endPosition(BinancePosition.LONG)
            trader.shortCoin()
        }

        lastSmallValue = valueSmall
        lastBigValue = valueBig
    }

    override fun getConfigurationDescription(): String {
        return "stepSmall: $stepSmall, stepBig: $stepBig"
    }
}