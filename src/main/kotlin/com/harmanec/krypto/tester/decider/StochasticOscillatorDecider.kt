package com.harmanec.krypto.tester.decider

import com.google.common.collect.EvictingQueue
import com.harmanec.krypto.calculator.StochasticOscillatorCalculator
import com.harmanec.krypto.decider.Decider
import com.harmanec.krypto.exchange.BinancePosition
import com.harmanec.krypto.exchange.Trader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.TestLogger
import java.math.BigDecimal

class StochasticOscillatorDecider(tickers: List<Ticker>,
                                  private val lowLine: Double,
                                  private val highLine: Double,
                                  private val step: Int,
                                  private val maStep: Int) : Decider {

    private val calculator = StochasticOscillatorCalculator(tickers.map { it.close }, tickers.map { it.low }, tickers.map { it.high }, step)
    private val ma: EvictingQueue<Double> = EvictingQueue.create(maStep)

    private var lastValue: Double = 0.0
    private var lastMaValue: Double = 0.0

    override var shouldLog: Boolean = false

    override fun priceChange(ticker: Ticker, trader: Trader) {
        val value = calculator.addPrice(ticker.close, ticker.low, ticker.high).toDouble()
        ma.add(value)
        val maValue = ma.average()

        if (shouldLog) {
            TestLogger.logText("so/indicator-value.csv", "${ticker.timestamp},$value,${ma.average()}")
        }

        if (value > highLine && maValue > highLine
            && ((lastValue >= lastMaValue && value <= maValue)
                    || lastValue <= lastMaValue && value >= maValue)) {
            trader.endPosition(BinancePosition.LONG)
            trader.shortCoin()
        } else if (value < lowLine && maValue < lowLine
            && ((lastValue >= lastMaValue && value <= maValue)
                    || lastValue <= lastMaValue && value >= maValue)) {
            trader.endPosition(BinancePosition.SHORT)
            trader.longCoin()
        }

        lastValue = value
        lastMaValue = maValue
    }

    override fun getConfigurationDescription(): String {
        return "Step: $step, maStep: $maStep, lowLine: $lowLine, highLine: $highLine"
    }
}