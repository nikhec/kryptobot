package com.harmanec.krypto.tester.decider

import com.harmanec.krypto.calculator.HullMovingAverageCalculator
import com.harmanec.krypto.decider.Decider
import com.harmanec.krypto.exchange.BinancePosition
import com.harmanec.krypto.exchange.Trader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.TestLogger
import com.harmanec.krypto.tester.WMACrossIndicatorValue
import java.math.BigDecimal

class HMovingAverageCrossDecider(prices: List<Ticker>,
                                 val stepSmall: Int,
                                 val stepBig: Int) : Decider {
    private val indicatorSmall: HullMovingAverageCalculator = HullMovingAverageCalculator(prices.map { it.close }, stepSmall)
    private val indicatorBig: HullMovingAverageCalculator = HullMovingAverageCalculator(prices.map { it.close }, stepBig)

    private var lastSmallValue: BigDecimal = BigDecimal.ZERO
    private var lastBigValue: BigDecimal = BigDecimal.ZERO
    private var longCross = true
    private var shortCross = true

    override var shouldLog: Boolean = false

    override fun priceChange(ticker: Ticker, trader: Trader) {
        val valueSmall = indicatorSmall.addPrice(ticker.close)
        val valueBig = indicatorBig.addPrice(ticker.close)
        if (shouldLog) {
            TestLogger.logCsv("hmaCross/indicator-value.csv", WMACrossIndicatorValue(ticker.timestamp, valueSmall, valueBig))
        }

        if ((lastSmallValue < lastBigValue) && (valueSmall > valueBig)) {
            longCross = true
            trader.endPosition(BinancePosition.SHORT)
            trader.longCoin()
        } else if ((lastSmallValue > lastBigValue) && (valueSmall < valueBig)) {
            shortCross = true
            trader.endPosition(BinancePosition.LONG)
            trader.shortCoin()
        }

        lastSmallValue = valueSmall
        lastBigValue = valueBig
    }

    override fun getConfigurationDescription(): String {
        return "StepSmall: $stepSmall, stepBig: $stepBig"
    }
}