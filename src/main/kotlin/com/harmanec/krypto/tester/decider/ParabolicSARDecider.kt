package com.harmanec.krypto.tester.decider

import com.harmanec.krypto.calculator.ParabolicSARCalculator
import com.harmanec.krypto.decider.Decider
import com.harmanec.krypto.exchange.BinancePosition
import com.harmanec.krypto.exchange.Trader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.TestLogger

class ParabolicSARDecider(tickers: List<Ticker>) : Decider {
    private val calculator = ParabolicSARCalculator(tickers.map { it.close }, tickers.map{ it.low }, tickers.map { it.high })

    override var shouldLog: Boolean = false

    override fun priceChange(ticker: Ticker, trader: Trader) {
        val value = calculator.addPrice(ticker)
        if (shouldLog) {
            TestLogger.logText("sar/indicator-value.csv", "${ticker.timestamp},$value")
        }

        if (calculator.trendChange) {
            if (value >= ticker.high) {
                trader.endPosition(BinancePosition.LONG)
                trader.shortCoin()
            } else {
                trader.endPosition(BinancePosition.SHORT)
                trader.longCoin()
            }
        }
    }
}