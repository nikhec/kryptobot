package com.harmanec.krypto.tester.decider

import com.harmanec.krypto.calculator.RSICalculator
import com.harmanec.krypto.decider.Decider
import com.harmanec.krypto.exchange.BinancePosition
import com.harmanec.krypto.exchange.Trader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.TestLogger

class RSIDecider(tickers: List<Ticker>,
                 private val lowLine: Double,
                 private val highLine: Double,
                 private val step: Int) : Decider {

    private var calculator: RSICalculator =
        RSICalculator(tickers.map { it.open }, tickers.map { it.close }, step)

    override var shouldLog: Boolean = false

    override fun priceChange(ticker: Ticker, trader: Trader) {
        val value = calculator.addPrice(ticker.open, ticker.close).toDouble()
        if (shouldLog) {
            TestLogger.logText("rsi/indicator-value.csv", "${ticker.timestamp},$value")
        }

        if (value >= highLine) {
            trader.endPosition(BinancePosition.LONG)
            trader.shortCoin()
        } else if (value <= lowLine) {
            trader.endPosition(BinancePosition.SHORT)
            trader.longCoin()
        }
    }

    override fun getConfigurationDescription(): String {
        return "Step: $step, lowLine: $lowLine, highLine: $highLine"
    }
}