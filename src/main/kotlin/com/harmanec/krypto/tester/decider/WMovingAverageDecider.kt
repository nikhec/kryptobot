package com.harmanec.krypto.tester.decider

import com.harmanec.krypto.calculator.WeightedMovingAverageCalculator
import com.harmanec.krypto.decider.Decider
import com.harmanec.krypto.exchange.BinancePosition
import com.harmanec.krypto.exchange.Trader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.TestLogger

class WMovingAverageDecider(tickers: List<Ticker>,
                            private val step: Int) : Decider {
    private val calculator = WeightedMovingAverageCalculator(
        tickers.map { it.close }.subList(0, Integer.min(step, tickers.size-1)),
        step
    )

    override var shouldLog: Boolean = false

    override fun priceChange(ticker: Ticker, trader: Trader) {
        val value = calculator.addPrice(ticker.close)
        if (shouldLog) {
            TestLogger.logText("wma/indicator-value.csv", "${ticker.timestamp},$value")
        }

        if (ticker.open.min(ticker.close) <= value && ticker.open.max(ticker.close) >= value) {
            if (ticker.open < ticker.close) {
                trader.endPosition(BinancePosition.LONG)
                trader.shortCoin()
            } else {
                trader.endPosition(BinancePosition.SHORT)
                trader.longCoin()
            }
        }
    }

    override fun getConfigurationDescription(): String {
        return "Step: $step"
    }
}
