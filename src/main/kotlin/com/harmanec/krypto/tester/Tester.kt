package com.harmanec.krypto.tester

import com.harmanec.krypto.decider.Decider
import com.harmanec.krypto.model.Ticker
import java.math.BigDecimal
import java.math.MathContext

class Tester private constructor(
    private val tickers: List<Ticker>,
    private val decider: Decider?,
    private val longProfit: Double,
    private val longStoploss: Double,
    private val shortProfit: Double,
    private val shortStoploss: Double,
    private val fee: Double,
    private val logFile: String?,
    private val wallet: TestWallet = TestWallet(),
    private val trader: TestTrader = TestTrader(longProfit, longStoploss, shortProfit, shortStoploss, fee, wallet, logFile)
) {
    fun test() : BigDecimal {
        decider?.let { decider ->
            for (ticker in tickers) {
                trader.priceChange(ticker)
                decider.priceChange(ticker, trader)
            }
        }
        val profit = wallet.usdt + (wallet.btc * tickers.last().close)

        return profit
    }

    data class Builder(
        var data: List<Ticker> = ArrayList(),
        var decider: Decider? = null,
        var longProfit: Double = 1.0,
        var longStoploss: Double = 1.0,
        var shortProfit: Double = 1.0,
        var shortStoploss: Double = 1.0,
        var logFile: String? = null,
        var fee: Double = 1.0
    ) {
        fun data(data: List<Ticker>) = apply { this.data = data }
        fun decider(decider: Decider) = apply { this.decider = decider }
        fun longStrategy(profit: Double, stoploss: Double) = apply {
            this.longProfit = profit
            this.longStoploss = stoploss
        }
        fun shortStrategy(profit: Double, stoploss: Double) = apply {
            this.shortProfit = profit
            this.shortStoploss = stoploss
        }
        fun logFile(log: String) = apply {
            this.logFile = log
        }
        fun fee(fee: Double) = apply { this.fee = fee }
        fun build() : Tester {
            return Tester(data, decider, longProfit, longStoploss, shortProfit, shortStoploss, fee, logFile)
        }
    }
}