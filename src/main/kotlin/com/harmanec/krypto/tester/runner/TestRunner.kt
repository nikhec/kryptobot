package com.harmanec.krypto.tester.runner

import com.harmanec.krypto.data.DataReader
import com.harmanec.krypto.decider.Decider
import com.harmanec.krypto.exchange.BinancePosition
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.*
import com.harmanec.krypto.utils.Utils
import java.math.BigDecimal
import java.util.*
import kotlin.collections.ArrayList

abstract class TestRunner(dataReader: DataReader) {
    private val tickers = dataReader.getHourlyTickers(1)
    protected var decider: Decider? = null
    private var logFile: String? = null

    val wallet = TestWallet()

    /**
     * Run tests
     *
     * @param learningPeriod learning period in months
     * @param testingPeriod testing period in months
     */
    fun runTests(
            learningPeriod: Int,
            testingPeriod: Int,
            longProfit: Double = 10.0,
            longStopLoss: Double = 0.0,
            shortProfit: Double = 0.0,
            shortStopLoss: Double = 10.0,
            fee: Double = 1.0,
            logFile: String? = null
    ) {
        this.logFile = logFile
        val learningPeriodHours = learningPeriod * 720
        val testingPeriodHours = testingPeriod * 720
        var iteration = 0
        var startIndex = 0
        var endIndex = learningPeriodHours + testingPeriodHours
        val sdf = java.text.SimpleDateFormat("yyyy-MM-dd")
        while (endIndex <= tickers.size) {
            val learningTickers = tickers.subList(
                    startIndex,
                    startIndex + learningPeriodHours
            )
            val testingTickers = tickers.subList(
                    startIndex + learningPeriodHours,
                    startIndex + learningPeriodHours + testingPeriodHours
            )

            var bestChunked = 1
            var bestProfit = BigDecimal(Double.MIN_VALUE)
            for (i in 1..12) {
                val learningTickersChunked = learningTickers.chunked(i).map { chunk ->
                    Utils.aggregateTickers(chunk)
                }
                val testProfit = runLearning(learningTickersChunked)
                if (testProfit > bestProfit) {
                    bestChunked = i
                    bestProfit = testProfit
                }
            }

            val date = Date(testingTickers.first().timestamp)
            println("Best period: $bestChunked, with best profit: $bestProfit")
            println("Best configuration: ${decider?.getConfigurationDescription()}")
            TestLogger.logText("$logFile/stats.txt", sdf.format(date))
            TestLogger.logText("$logFile/stats.txt", "Best period: $bestChunked, with best profit: $bestProfit")
            TestLogger.logText("$logFile/stats.txt", "Best configuration: ${decider?.getConfigurationDescription()}")

            val stopLossTickers = learningTickers.chunked(bestChunked).map {
                Utils.aggregateTickers(it)
            }
            var bestStopLossProfit = BigDecimal.ZERO
            var bestLongStopLoss = 0
            var bestShortStopLoss = 0
            for (stopLossLong in 1..100) {
                for (stopLossShort in 1..100) {
                    wallet.reset()
                    val stopLossTrader = TestTrader(longProfit, (1 - (stopLossLong.toDouble() * 0.01)) , shortProfit, (1 + (stopLossShort.toDouble() * 0.01)), fee, wallet, null)
                    for(ticker in stopLossTickers) {
                        stopLossTrader.priceChange(ticker)
                        decider?.priceChange(ticker, stopLossTrader)
                    }
                    stopLossTrader.endPosition(BinancePosition.LONG)
                    stopLossTrader.endPosition(BinancePosition.SHORT)
                    if(wallet.usdt > bestStopLossProfit) {
                        bestStopLossProfit = wallet.usdt
                        bestLongStopLoss = stopLossLong
                        bestShortStopLoss = stopLossShort
                    }
                }
            }
            println("$logFile: Best long stopLoss: ${1 + (bestLongStopLoss.toDouble() * 0.01)}, short stopLoss: ${1 - (bestShortStopLoss.toDouble() * 0.01)}%, profit: $bestStopLossProfit")
            TestLogger.logText("$logFile/stoploss.csv", "${sdf.format(date)}\n$bestLongStopLoss,$bestShortStopLoss,$bestStopLossProfit")

            wallet.reset()
            val testingTrader = TestTrader(longProfit, 1 - (bestLongStopLoss.toDouble() * 0.01), shortProfit, 1 + (bestShortStopLoss.toDouble() * 0.01), fee, wallet, "$logFile/trades.csv")
            val testingTickersChunked = testingTickers.chunked(bestChunked).map {
                Utils.aggregateTickers(it)
            }
            runTesting(testingTickersChunked, testingTrader)

            TestLogger.logText("$logFile/btc-price.csv", "")
            TestLogger.logText("$logFile/budget.csv", "")
            TestLogger.logText("$logFile/indicator-value.csv", "")
            TestLogger.logText("$logFile/trades.csv", "")

            iteration++
            startIndex = iteration * testingPeriodHours
            endIndex = startIndex + learningPeriodHours + testingPeriodHours

            println(sdf.format(date))
            println("wallet: $${wallet.getResourcesInDollars(testingTickersChunked.last().close)}")
        }
    }

    /**
     * Run learning on given tickers and return the best profit
     */
    abstract fun runLearning(tickers: List<Ticker>): BigDecimal

    /**
     * Run tests on given tickers (best period on previous testing)
     * @param logger the logger where should be logged all trades
     */
    private fun runTesting(tickers: List<Ticker>, trader: TestTrader) {
        println("Running tests..")
        decider?.shouldLog = true
        for (ticker in tickers) {
            trader.priceChange(ticker)
            decider?.priceChange(ticker, trader)
            TestLogger.logCsv("$logFile/btc-price.csv", TickerValue(ticker))
            TestLogger.logCsv("$logFile/budget.csv", WalletBudget(ticker.timestamp, wallet.getResourcesInDollars(ticker.close)))
        }
        trader.endPosition(BinancePosition.LONG)
        trader.endPosition(BinancePosition.SHORT)
        decider?.shouldLog = false
    }
}