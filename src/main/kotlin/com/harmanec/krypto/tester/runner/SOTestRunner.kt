package com.harmanec.krypto.tester.runner

import com.harmanec.krypto.data.DataReader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.Tester
import com.harmanec.krypto.tester.decider.StochasticOscillatorDecider
import java.math.BigDecimal

class SOTestRunner(dataReader: DataReader): TestRunner(dataReader) {
    override fun runLearning(tickers: List<Ticker>): BigDecimal {
        var bestProfit = BigDecimal(Double.MIN_VALUE)
        for(maStep in 3..11 step 2) {
            for (step in 10..20 step 2) {
                for (lowLine in 1..30 step 2) {
                    for (highLine in 70..99 step 2) {
                        val decider =
                            StochasticOscillatorDecider(tickers, lowLine.toDouble(), highLine.toDouble(), step, maStep)
                        val tester = Tester.Builder()
                            .data(tickers)
                            .decider(decider)
                            .fee(0.995)
                            .build()

                        val profit = tester.test()
                        if (profit > bestProfit) {
                            bestProfit = profit
                            this.decider = decider
                            println("SO --- step: $step, lowLine: $lowLine, highLine: $highLine, profit: $profit")
                        }
                    }
                }
            }
        }

        return bestProfit
    }
}