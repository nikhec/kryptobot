package com.harmanec.krypto.tester.runner

import com.harmanec.krypto.data.DataReader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.TestLogger
import com.harmanec.krypto.tester.Tester
import com.harmanec.krypto.tester.decider.WMovingAverageCrossDecider
import java.math.BigDecimal

class WMACrossTestRunner(dataReader: DataReader): TestRunner(dataReader) {
    private var tickerLength = 1
    override fun runLearning(tickers: List<Ticker>): BigDecimal {
        var bestProfit = BigDecimal(Double.MIN_VALUE)
        for (stepSmall in 1..50) {
            for (stepBig in (stepSmall + 10)..100) {
                val decider = WMovingAverageCrossDecider(tickers, stepSmall, stepBig)
                val tester = Tester.Builder()
                    .data(tickers)
                    .decider(decider)
                    .fee(0.995)
                    .build()

                val profit = tester.test()
                if (profit > bestProfit) {
                    bestProfit = profit
                    this.decider = decider
                }
            }
        }

        tickerLength++
        return bestProfit
    }
}