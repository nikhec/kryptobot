package com.harmanec.krypto.tester.runner

import com.harmanec.krypto.data.DataReader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.Tester
import com.harmanec.krypto.tester.decider.MovingAverageDecider
import java.math.BigDecimal

class MATestRunner(dataReader: DataReader) : TestRunner(dataReader) {

    override fun runLearning(tickers: List<Ticker>): BigDecimal {
        var bestProfit = BigDecimal(Double.MIN_VALUE)
        for (step in 1..100) {
            val decider = MovingAverageDecider(tickers, step)
            val tester = Tester.Builder()
                .data(tickers)
                .decider(decider)
                .fee(0.995)
                .build()

            val profit = tester.test()
            if (profit > bestProfit) {
                bestProfit = profit
                this.decider = decider
            }
        }

        return bestProfit
    }
}