package com.harmanec.krypto.tester.runner

import com.harmanec.krypto.tester.decider.HMovingAverageCrossDecider
import com.harmanec.krypto.data.DataReader
import com.harmanec.krypto.model.Ticker
import com.harmanec.krypto.tester.Tester
import java.math.BigDecimal

class HMACrossTestRunner(dataReader: DataReader): TestRunner(dataReader) {
    override fun runLearning(tickers: List<Ticker>): BigDecimal {
        var bestProfit = BigDecimal(Double.MIN_VALUE)
        for (stepSmall in 2..50 step 2) {
            for (stepBig in (stepSmall + 10)..100 step 2) {
                val decider = HMovingAverageCrossDecider(tickers, stepSmall, stepBig)
                val tester = Tester.Builder()
                    .data(tickers)
                    .decider(decider)
                    .fee(0.995)
                    .build()

                val profit = tester.test()
                if (profit > bestProfit) {
                    bestProfit = profit
                    this.decider = decider
                }
            }
        }

        return bestProfit
    }
}