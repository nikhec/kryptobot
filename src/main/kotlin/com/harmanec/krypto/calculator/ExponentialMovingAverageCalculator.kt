package com.harmanec.krypto.calculator

import com.harmanec.krypto.indicator.calculator.MovingAverageCalculator
import java.math.BigDecimal
import java.math.MathContext
import java.math.RoundingMode
import kotlin.math.min

/**
 * Exponential moving average Calculator
 *
 * Calculates the exponential moving average
 *
 * @property prices list of history prices
 * @property step the length on which the moving average should be calculated
 * @property smoothing the smoothing
 */
class ExponentialMovingAverageCalculator(prices: List<BigDecimal>, private val step: Int, private val smoothing: Int = 2) {
    private val movingAverageCalculator = MovingAverageCalculator(prices.subList(0, min(step, prices.size)), step)
    private val exponentialMovingAverages : MutableList<BigDecimal> = ArrayList()

    init {
        require(step <= prices.size)

        exponentialMovingAverages.add(calculateEMA(prices[step-1], movingAverageCalculator.getAll().last()))
        for (i in step until prices.size) {
            addPrice(prices[i])
        }
    }

    fun addPrice(price: BigDecimal) : BigDecimal {
        val ema = calculateEMA(price, exponentialMovingAverages.last())
        exponentialMovingAverages.add(ema)

        return ema
    }

    fun getAll() : List<BigDecimal> {
        return exponentialMovingAverages
    }

    private fun calculateEMA(price: BigDecimal, lastEMA: BigDecimal) : BigDecimal {
        return (price.multiply(BigDecimal(smoothing).divide(BigDecimal(1 + step), MathContext.DECIMAL32)) +
                lastEMA.multiply(BigDecimal.ONE - BigDecimal(smoothing).divide(BigDecimal(1 + step), MathContext.DECIMAL32)))
            .setScale(10, RoundingMode.HALF_UP)
    }
}