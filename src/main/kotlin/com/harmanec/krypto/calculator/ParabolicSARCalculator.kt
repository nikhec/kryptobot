package com.harmanec.krypto.calculator

import com.harmanec.krypto.model.Ticker
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Parabolic SAR Calculator
 *
 * Calculates the Parabolic SAR
 *
 * @property closePrices list of history close prices
 * @property lowPrices list of history low prices
 * @property highPrices list of history high prices
 * @property accelerationStep the acceleration step
 * @property accelerationMax the acceleration max
 */
class ParabolicSARCalculator(closePrices: List<BigDecimal>, lowPrices: List<BigDecimal>, highPrices: List<BigDecimal>, private val accelerationStep : Double = 0.02, private val accelerationMax : Double = 0.2 ) {
    private val parabolicSARs : MutableList<BigDecimal> =  ArrayList()
    private var extreme : BigDecimal = BigDecimal.ZERO
    private var acceleration : Double = accelerationStep
    private var lastRising : Boolean = false
    var trendChange: Boolean = false

    init {
        lastRising = (highPrices[1] >= highPrices[0] || lowPrices[0] <= lowPrices[1])
        extreme = if (lastRising) highPrices[0] else lowPrices[0]
        parabolicSARs.add(extreme)

        for (i in 1 until lowPrices.size) {
            addPrice(Ticker(BigDecimal.ZERO, lowPrices[i], highPrices[i], closePrices[i], BigDecimal.ZERO, 0, 0))
        }
    }

    fun addPrice(price: Ticker) : BigDecimal {
        var sar = calculateParabolicSAR(lastRising, parabolicSARs.last())
        val rising = if (lastRising) sar < price.close else sar < price.close

        if (rising != lastRising) {
            acceleration = accelerationStep
            sar = extreme
            extreme = if (rising) price.high else price.low
        }
        trendChange = rising != lastRising
        if (rising && price.high > extreme) {
            if (acceleration < accelerationMax) acceleration += accelerationStep
            extreme = price.high
        } else if (!rising && price.low < extreme) {
            if (acceleration < accelerationMax) acceleration += accelerationStep
            extreme = price.low
        }

        parabolicSARs.add(sar)
        lastRising = rising

        return sar
    }

    fun getAll() : List<BigDecimal> {
        return parabolicSARs
    }

    private fun calculateParabolicSAR(rising: Boolean, last: BigDecimal) : BigDecimal {
        return if (rising) {
            (last + (acceleration.toBigDecimal() * (extreme - last))).setScale(10, RoundingMode.HALF_UP)
        } else {
            (last - (acceleration.toBigDecimal() * (last - extreme))).setScale(10, RoundingMode.HALF_UP)
        }
    }
}