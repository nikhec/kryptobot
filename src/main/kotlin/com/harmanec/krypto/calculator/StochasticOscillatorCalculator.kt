package com.harmanec.krypto.calculator

import com.google.common.collect.EvictingQueue
import java.math.BigDecimal
import java.math.MathContext

/**
 * Stochastic oscillator Calculator
 *
 * Calculates the stochastic oscillator
 *
 * @property closingPrices list of history closing prices
 * @property lowestPrices list of history lowest prices
 * @property highestPrices list of history highest prices
 * @property step the length on which the value should be calculated
 */
class StochasticOscillatorCalculator(closingPrices: List<BigDecimal>, lowestPrices: List<BigDecimal>, highestPrices: List<BigDecimal>, step: Int) {
    private val oscillator: MutableList<BigDecimal> = ArrayList()
    private val lastLowestPrices: EvictingQueue<BigDecimal> = EvictingQueue.create(step)
    private val lastHighestPrices: EvictingQueue<BigDecimal> = EvictingQueue.create(step)

    init {
        for (i in closingPrices.indices) {
            lastLowestPrices.add(lowestPrices[i])
            lastHighestPrices.add(highestPrices[i])
            if (i >= step) {
                oscillator.add(calculateOscillator(closingPrices[i]))
            }
        }
    }

    private fun calculateOscillator(closingPrice: BigDecimal) : BigDecimal {
        val lowestPrice = lastLowestPrices.minOrNull() ?: BigDecimal.ZERO
        val highestPrice = lastHighestPrices.maxOrNull() ?: BigDecimal.ONE

        if (highestPrice.compareTo(lowestPrice) == 0) {
            return BigDecimal(50)
        }

        return ((closingPrice - lowestPrice).divide(highestPrice - lowestPrice, MathContext.DECIMAL128)).times(BigDecimal(100))
    }

    fun addPrice(closingPrice: BigDecimal, lowestPrice: BigDecimal, highestPrice: BigDecimal) : BigDecimal {
        lastLowestPrices.add(lowestPrice)
        lastHighestPrices.add(highestPrice)
        val value = calculateOscillator(closingPrice)
        oscillator.add(value)

        return value
    }

    fun getAll() : List<BigDecimal> {
        return oscillator
    }
}