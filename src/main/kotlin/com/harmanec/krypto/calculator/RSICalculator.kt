package com.harmanec.krypto.calculator

import com.google.common.collect.EvictingQueue
import com.harmanec.krypto.indicator.calculator.average
import java.math.BigDecimal
import java.math.MathContext

data class RSIPriceBar(val upMove: Boolean, val price: BigDecimal)

/**
 * RSI Calculator
 *
 * Calculates the RSI
 *
 * @property openPrices list of history open prices
 * @property closePrices list of history close prices
 * @property step the length on which the value should be calculated
 */
class RSICalculator(openPrices: List<BigDecimal>, closePrices: List<BigDecimal>, step: Int) {
    private val RSIValues : MutableList<BigDecimal> = ArrayList()
    private val lastPrices: EvictingQueue<RSIPriceBar> = EvictingQueue.create(step)

    init {
        for (i in openPrices.indices) {
            val priceBar = RSIPriceBar(closePrices[i] > openPrices[i], (closePrices[i] - openPrices[i]).abs())
            lastPrices.add(priceBar)
            if (i >= step) {
                RSIValues.add(calculateRSI())
            }
        }
    }

    private fun calculateRSI(): BigDecimal {
        val avgU = lastPrices.filter { it.upMove }
            .map { it.price }
            .average()
        val avgD = lastPrices.filter { !it.upMove }
            .map { it.price }
            .average()
            .max(BigDecimal.ONE)

        return BigDecimal(100) - BigDecimal(100).divide(
            BigDecimal.ONE + (avgU.divide(avgD, MathContext.DECIMAL128)),
            MathContext.DECIMAL128
        )
    }

    fun addPrice(openPrice: BigDecimal, closePrice: BigDecimal) : BigDecimal {
        lastPrices.add(RSIPriceBar(closePrice > openPrice, (closePrice - openPrice).abs()))
        val rsi = calculateRSI()
        RSIValues.add(rsi)

        return rsi
    }

    fun getAll() : List<BigDecimal> {
        return RSIValues
    }
}