package com.harmanec.krypto.calculator

import com.google.common.collect.EvictingQueue
import java.lang.Integer.min
import java.math.BigDecimal
import kotlin.math.max

/**
 * Weighted Moving Average Calculator
 *
 * Calculates the weighted moving average
 *
 * @property data list of history prices
 * @property step the length on which the value should be calculated
 */
class WeightedMovingAverageCalculator(data: List<BigDecimal>, step: Int) {
    private val lastPrices: EvictingQueue<BigDecimal> = EvictingQueue.create(step)
    private val WMAs: MutableList<BigDecimal> = ArrayList()

    init {
        for (i in 0 until min(step, data.size)) {
            lastPrices.add(data[i])
        }

        for (i in min(step, data.size) until data.size) {
            addPrice(data[i])
        }
    }

    fun addPrice(price: BigDecimal) : BigDecimal {
        lastPrices.add(price)

        val wma = calculateWMA()
        WMAs.add(wma)

        return wma
    }

    fun getAll() : List<BigDecimal> {
        return WMAs
    }

    private fun calculateWMA() : BigDecimal {
        var weight = 1
        var sumWeights = 0
        var WMA = BigDecimal.ZERO
        for (price in lastPrices) {
            WMA += price * weight.toBigDecimal()
            sumWeights += weight
            weight++
        }

        return WMA / max(1, sumWeights).toBigDecimal()
    }
}