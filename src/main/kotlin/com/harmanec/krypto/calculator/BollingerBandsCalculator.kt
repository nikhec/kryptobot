package com.harmanec.krypto.calculator

import com.google.common.collect.EvictingQueue
import com.harmanec.krypto.indicator.calculator.MovingAverageCalculator
import com.harmanec.krypto.indicator.calculator.average
import java.math.BigDecimal
import kotlin.math.pow
import kotlin.math.sqrt

/**
 * Bollinger bands Calculator
 *
 * Calculates the bollinger bands
 *
 * @property prices list of history prices
 * @property step the length on which the moving average should be calculated
 * @property multiplicator the multiplicator
 */
class BollingerBandsCalculator(prices: List<BigDecimal>, private val step: Int, private val multiplicator: Int) {
    private val movingAverageCalculator = MovingAverageCalculator(prices.subList(0, step), step)
    private val bollingerBands: MutableList<BollingerBand> = ArrayList()
    private val lastPrices: EvictingQueue<BigDecimal> = EvictingQueue.create(step)

    init {
        require(step <= prices.size)

        for (i in 0 until step) {
            lastPrices.add(prices[i])
        }

        for (i in step until prices.size) {
            addPrice(prices[i])
        }
    }

    fun addPrice(prices: BigDecimal) : BollingerBand {
        val ma = movingAverageCalculator.addPrice(prices).toDouble()
        lastPrices.add(prices)
        val deviation = calculateDeviation() * multiplicator
        val bollingerBand = BollingerBand(ma, ma + deviation, ma - deviation)
        bollingerBands.add(bollingerBand)

        return bollingerBand
    }

    fun getAll() : List<BollingerBand> {
        return bollingerBands
    }

    private fun calculateDeviation() : Double {
        val mean = lastPrices.average().toDouble()
        var deviationsSum = 0.0
        lastPrices.forEach { price ->
            deviationsSum += (price.toDouble() - mean).pow(2)
        }

        return sqrt(deviationsSum / step)
    }
}

data class BollingerBand(val movingAverage : Double, val upperBand : Double, val lowerBand: Double)