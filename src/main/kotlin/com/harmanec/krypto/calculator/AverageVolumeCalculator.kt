package com.harmanec.krypto.calculator

import com.google.common.collect.EvictingQueue
import com.harmanec.krypto.indicator.calculator.average
import java.lang.Integer.min
import java.math.BigDecimal

/**
 * Average volume Calculator
 *
 * Calculates the average volume
 *
 * @property data list of historical volumes
 * @property step the length on which the average should be calculated
 */
class AverageVolumeCalculator(data: List<BigDecimal>, step: Int) {
    private val volumes : EvictingQueue<BigDecimal> = EvictingQueue.create(step)
    private val averages: MutableList<BigDecimal> = ArrayList()

    init {
        for (i in 0 until min(step, data.size)) {
            volumes.add(data[i])
        }

        for (i in min(step, data.size) until data.size) {
            addVolume(data[i])
        }
    }

    fun addVolume(volume: BigDecimal) : BigDecimal {
        volumes.add(volume)
        val avg = volumes.average()
        averages.add(avg)

        return avg
    }

    fun getAll() : List<BigDecimal> {
        return averages
    }
}