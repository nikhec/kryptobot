package com.harmanec.krypto.calculator

import java.math.BigDecimal
import kotlin.math.sqrt

/**
 * Hull moving average Calculator
 *
 * Calculates the hull moving average
 *
 * @property prices list of history prices
 * @property step the length on which the moving average should be calculated
 */
class HullMovingAverageCalculator(prices: List<BigDecimal>, step: Int) {
    private val fullWMA = WeightedMovingAverageCalculator(ArrayList(), step)
    private val halfWMA = WeightedMovingAverageCalculator(ArrayList(), step / 2)
    private val hma = WeightedMovingAverageCalculator(ArrayList(), sqrt(step.toDouble()).toInt())

    init {
        for (price in prices) {
            addPrice(price)
        }
    }

    fun addPrice(price: BigDecimal) : BigDecimal {
        val full = fullWMA.addPrice(price)
        val half = halfWMA.addPrice(price) * BigDecimal(2)

        return hma.addPrice(half - full)
    }

    fun getAll() : List<BigDecimal> {
        return hma.getAll()
    }
}