package com.harmanec.krypto.indicator.calculator

import com.google.common.collect.EvictingQueue
import java.lang.Integer.max
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.MathContext

/**
 * Moving average Calculator
 *
 * Calculates the moving average
 *
 * @property prices list of history prices
 * @property step the length on which the moving average should be calculated
 */
class MovingAverageCalculator(prices: List<BigDecimal>, step: Int) {
    private val movingAverages : MutableList<BigDecimal> = ArrayList()
    private val lastPrices : EvictingQueue<BigDecimal> = EvictingQueue.create(step)

    init {
        movingAverages.addAll(prices.windowed(step, 1, false) { it.average() })
        lastPrices.addAll(prices.subList(max(0, prices.lastIndex - step), prices.lastIndex + 1))
    }

    fun addPrice(price: BigDecimal) : BigDecimal {
        lastPrices.add(price)
        val value = lastPrices.average()
        movingAverages.add(value)

        return value
    }

    fun getAll() : MutableList<BigDecimal> {
        return movingAverages
    }
}

fun <E> EvictingQueue<E>.average() : BigDecimal {
    if (this.size > 0) {
        val sum = this.fold(ZERO, BigDecimal::add)
        return sum.divide(BigDecimal(this.size), MathContext.DECIMAL128)
    }

    return ZERO
}

fun <E> List<E>.average() : BigDecimal {
    if (this.isNotEmpty()) {
        val sum = this.fold(ZERO, BigDecimal::add)
        return sum.divide(BigDecimal(this.size), MathContext.DECIMAL128)
    }

    return ZERO
}

fun <E> BigDecimal.add(e: E): BigDecimal {
    return this + (e as BigDecimal)
}