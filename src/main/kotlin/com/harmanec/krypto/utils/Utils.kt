package com.harmanec.krypto.utils

import com.harmanec.krypto.model.Ticker
import java.math.BigDecimal

class Utils {
    companion object {
        /**
         * Aggregates list of Tickers into one Ticker
         */
        fun aggregateTickers(tickers: List<Ticker>) : Ticker {
            val open = tickers.first().open
            var low = BigDecimal(Double.MAX_VALUE)
            var high = BigDecimal.ZERO
            val close = tickers.last().close
            var volume = BigDecimal.ZERO
            var trades = 0

            tickers.forEach { ticker ->
                low = low.min(ticker.low)
                high = high.max(ticker.high)
                volume += ticker.volume
                trades += ticker.trades
            }

            return Ticker(open, low, high, close, volume, trades, tickers.last().timestamp)
        }
    }
}